from django.contrib import admin

from .models import Dataset


class DatasetAdmin( admin.ModelAdmin ):
	list_display = ( 'title', 'slug', 'description', 'database', 'added' )
	search_fields = ('title', 'database', 'added' )
	prepopulated_fields={'slug':('title',)}
	#date_hierarchy='added'
	ordering =['title', 'database', 'added' ]
	
# Register your models here.
admin.site.register(Dataset, DatasetAdmin)