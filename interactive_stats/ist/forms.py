from django import forms

class DataSelectionForm( forms.Form ):

    measures = forms.MultipleChoiceField( label = "Measures")

    def __init__( self, *args, **kwargs ):

        measures_list = kwargs.pop('measures_list')
        filters_map   = kwargs.pop('filters')

        super( DataSelectionForm, self ).__init__(*args, **kwargs)

        measure_choices = []
        for measure in measures_list:
            measure_choices.append( ( measure, measure ) )

        self.fields['measures'].choices = measure_choices
        self.fields['measures'].widget.attrs['class'] = 'selectpicker'
        self.fields['measures'].widget.attrs['multiple'] = 'True'

        for i, filter in enumerate( filters_map ):

            feild_name = 'filter_{}'.format( i )
            self.fields[ feild_name ] = forms.MultipleChoiceField( label = filter, required = False )
            self.fields[ feild_name ].widget.attrs['class'] = 'selectpicker'
            self.fields[ feild_name ].widget.attrs['multiple'] = 'True'

            filter_choice = []
            for choice in filters_map[ filter ]:
                filter_choice.append( ( choice, choice ) )

            self.fields[ feild_name ].choices = filter_choice
