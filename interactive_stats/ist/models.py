from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse,resolve


# Create your models here.
class Dataset( models.Model ):

    ## title of the dataset
	title       = models.CharField( max_length = 250, primary_key = True )
    
    ## slug used for SEO friendly urls
	slug        = models.SlugField( max_length = 250, unique_for_date ='added' )
    
    ## date the dataset was added
	added       = models.DateTimeField( auto_now_add = True )

    ## dataset description
	description = models.TextField( default = "" )
    
    ## database details. should include table
	database    = models.CharField( max_length = 250, blank = False )
    
    ## list of data measures
	measures    = models.TextField( help_text = "comma seperated list of measure coloumn names" )
    
    ## list of data filters 
	filters     = models.TextField( help_text = "comma seperated list of filter coloumn names" )

    ## list of all available objects
	objects     = models.Manager()

	class Meta:
		ordering = ( '-title', )

	def __str__( self ):
		return self.title

	def get_details_url( self ):
		return reverse( 'ist:dataset_details', None, [str(self.slug)])

	def get_data_url( self, data ):
		return reverse( 'ist:dataset_get_data', None, [data])

	def get_measures( self ):
		return self.measures.split(',')

	def get_filters( self ):
		return self.filters.split(',')

	def get_object_count( self ):
		return len( self.objects.all() )

