from django.conf.urls import url, include
from django.conf import settings
from . import views


urlpatterns = [ url( r'^$', views.index, name='index' ) 
              , url( r'^about/', views.about,name='about' )
              , url( r'^contact/', views.contact, name='contact' )
              , url( r'^dataset_detail/(?P<id>[-\w]+)/$', views.dataset_details, name='dataset_details' )
              , url( r'^dataset_get_data/$', views.dataset_get_data, name='dataset_get_data' )
              , url( r'^media/(?P<file_path>[-\w\.]+)'
                   , views.data_download_handler
                   , name = 'data_download_handler'
                   )
]
