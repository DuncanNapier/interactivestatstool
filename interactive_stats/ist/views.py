from django.shortcuts import render, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponse
#from django.backends.base import SessionBase
from django.core.files import File
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile

from .models import Dataset
from .forms import DataSelectionForm

import base64
import csv
import random
import urllib

# Create your views here.

def test_options():
    
    lists = []
    
    lists.append( [1,2,3,4,5] )  
    lists.append( [ "mon", "tue", "wed", "thurs", "fri" ] )  
    lists.append( [ "16-25", "26-35", "36-55", "56-65", "66+" ] )  
    
    ix = random.randint( 0, len( lists ) - 1 )
    return lists[1]
    
def test_table_data():
    data = []
    data.append( [ 'Title A', 'Title B', 'Title C', 'Title D', 'Title E' , 'Title F'  ] )
    for i in range( 100 ):
        row = []
        for i in range( len( data[0] ) ):
            row.append( float( random.randint( 0, 1000 ) ) / 1000 )
        data.append( row )
    return data
    
    
def data_download_handler( request, file_path ):
    f = default_storage.open( file_path )
    response = HttpResponse(content_type='text/plain')
    response['Content-Disposition'] = 'attachment; filename="{}"'.format( file_path )
    response.write( f.read() )
    return response
 
## index view 
def index( request ):
    datasets = Dataset.objects.all()
    return render( request
                 , 'ist/index.html'
                 , { 'nav_page':'index'
                   , 'datasets': datasets  } )

## view of an idividual dataset
def dataset_details( request, id ):
    dataset = get_object_or_404( Dataset, slug = id )
    
    measures = dataset.measures.split(',')
    filters = dataset.filters.split(',')
    
    filter_map = {}
    for filter in filters:
        filter_map[filter] = test_options()
        
    file_response = None
    table_data    = None
    encoded_data  = None
    data_file_path = ''
    if request.method == 'POST':
        select_form = DataSelectionForm( measures_list = measures, filters = filter_map, data = request.POST )
        
        if select_form.is_valid():
            cd = select_form.cleaned_data    
            table_data = test_table_data()
            
            # create text if a downloaded is required
            text = ''
            for row in table_data:
                string_list = []
                for value in row:
                    string_list.append( str(value) ) 
                text += ", ".join( string_list )
                text += '\n'
            data_file_path = default_storage.save('data.csv', ContentFile(text))
            data_file_path =  "/" + default_storage.url( data_file_path )
    else:
        select_form = DataSelectionForm( measures_list = measures, filters = filter_map )
        
    return render( request
                 , 'ist/dataset_details.html'
                 , context={ 'dataset': dataset
                           , 'select_form': select_form
                           , 'table_data': table_data
                           , 'data_file_path': data_file_path } )
                           
def dataset_get_data( request ):
    text = request.session['data']
    return writeCSVFileResponse( 'data.csv', text  )
    
    
## view for the about details
def about( request ):
    return render( request
                 , 'ist/about.html'
                 , {'nav_page':'about'} )

 ## view for the contact details
def contact( request ):
    return render( request
                , 'ist/contact.html'
                , {'nav_page':'contact' } )
    